import { Button } from "@/app/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/app/components/ui/dialog";
import { Label } from "@/app/components/ui/label";
import { useContext, useState } from "react";
import { EditPostContext } from "../context/EditPostContext";
import { clientServices } from "@/app/services/client.services";
import SingleImageUpload from "@/app/components/themes/ImageUpload/SingleImageUpload";
import { NotificationContext } from "@/app/providers/NotificationProvider";
import { ReloadIcon } from "@radix-ui/react-icons";

const UpdatePostThumbnailDialog = () => {
  const { onCloseModal, post, handleGetPostDetail } =
    useContext(EditPostContext);
  const [file, setFile] = useState<File | null>(null);
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [loading, setLoading] = useState<boolean>(false);

  const handleSubmit = async () => {
    if (!post || !file) return;
    try {
      const res = await clientServices.updatePost({
        id: post.id,
        thumbnail: file,
      });
      if (res.data) {
        openNotificationWithIcon(
          "success",
          "Cập nhật thành công",
          "Thông tin bài viết đã được cập nhật"
        );
        handleGetPostDetail?.(post.id?.toString());
        onCloseModal?.();
        setLoading(false);
      }
    } catch (error) {
      console.log("error", error);
      openNotificationWithIcon(
        "error",
        "Thất bại",
        "Thông tin bài viết cập nhật thất bại!"
      );
      setLoading(false);
    }
  };

  return (
    <Dialog open={true} onOpenChange={() => onCloseModal?.()}>
      <DialogContent className="max-w-[60vw]">
        <DialogHeader>
          <DialogTitle>Cập nhật thông tin bài viết</DialogTitle>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-4 items-center gap-4">
            <Label className="text-right">Hình ảnh tiêu đề</Label>
            <div className="col-span-3">
              <SingleImageUpload
                onFileUpload={(file) => {
                  setFile(file);
                }}
                onDeleteImage={() => {
                  setFile(null);
                }}
              />
            </div>
          </div>
        </div>
        <DialogFooter>
          <Button
            type="button"
            className="bg-primary-color hover:bg-[#108a00]/80"
            onClick={() => onCloseModal?.()}
          >
            Đóng
          </Button>
          <Button
            disabled={loading}
            className="bg-primary-color"
            onClick={() => handleSubmit()}
          >
            {loading && (
              <ReloadIcon className="mr-2 h-4 w-4 animate-spin inline-flex" />
            )}
            Cập nhật
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default UpdatePostThumbnailDialog;
