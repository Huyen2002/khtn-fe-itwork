import { Button,  Result } from "antd";
import Image from "next/image";
import React from "react";

const NotInfo = () => {
  return (
    <section>
     <div style={{ display: "flex", justifyContent: "center" }}>
              <Image
                alt="img"
                className="object-contain"
                src="https://timviecits.id.vn/storage/nodata.webp"
                width={300}
                height={300}
              />
              {/* <img
                style={{ width: 300 }}
               
              ></img> */}
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                fontSize: 25,
                marginTop: 30,
                fontWeight: 600,
              }}
            >
              Chưa có thông tin hợp đồng nào
            </div>
    </section>
  );
};

export default NotInfo;
