import { UserList } from '../types/authentication.types';
import { CommonResponse } from '../types/common.types';
import { ReportList, ReportStatus } from '../types/reports.type';
import ApiService from './ApiService';

// User
type GetUsersRequest = {
  page: number;
  num: number;
  search?: string;
  status?: string;
};

type GetUsersResponse = CommonResponse & {
  data: {
    current_page: number;
    data: UserList[];
    num: number;
    total: number;
    total_page: number;
  };
};

// Reports
type GetReportsRequest = {
  page: number;
  num: number;
  status: ReportStatus | undefined;
};

type GetReportsResponse = CommonResponse & {
  data: {
    current_page: number;
    data: ReportList[];
    num: number;
    total: number;
    total_page: number;
  };
};

type ResolveReportRequest = {
  results: string;
  resolve: ReportStatus;
};

// Freelancers
export function GetAllFreelancer(params: GetUsersRequest) {
  return ApiService.get<GetUsersResponse>(`/administrator/freelancer`, params);
}

export function UpdateFreelancer(id: number, status: number) {
  return ApiService.put<CommonResponse>(`/administrator/freelancer/${id}`, {
    status,
  });
}

// Clients
export function GetAllClient(params: GetUsersRequest) {
  return ApiService.get<GetUsersResponse>(`/administrator/client`, params);
}

export function UpdateClient(id: number, status: number) {
  return ApiService.put<CommonResponse>(`/administrator/client/${id}`, {
    status,
  });
}

// Reports
export function GetAllReports(params: GetReportsRequest) {
  return ApiService.get<GetReportsResponse>(`/administrator/report`, params);
}

export function ResolveReports(id: number, params: ResolveReportRequest) {
  return ApiService.put<CommonResponse>(
    `/administrator/report/resolve/${id}`,
    params
  );
}
