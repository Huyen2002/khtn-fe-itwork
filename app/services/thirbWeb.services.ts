import { ContractStatus } from '../types/contract.type';

interface IContractDetail {
  contract_id: number;
  title: string;
  description: string;
  signature_freelancer: string;
  signature_client: string;
  bids: string;
  status: number;
  address_client: number;
  address_freelancer: number;
  freelancer_id: number;
  client_id: number;
}

export interface IContractList {
  id: number;
  jobIdcurent: number;
  title: string;
  status: ContractStatus;
}

const getDetailContractByJobId = async (
  jobId: number,
  contract: any
): Promise<IContractDetail | null> => {
  try {
    const data = await contract?.call('getJobInfoByCurrentJobId', [jobId]);
    return {
      contract_id: data[0].toNumber(),
      title: data[1],
      description: data[2],
      signature_freelancer: data[3],
      signature_client: data[4],
      bids: data[5].toNumber(),
      status: data[6],
      address_client: data[7],
      address_freelancer: data[8],
      freelancer_id: parseInt(data[9]),
      client_id: parseInt(data[10]),
    };
  } catch (err) {
    return null;
  }
};

const getContractsByFreelancerId = async (
  freelancerId: number,
  contract: any
): Promise<IContractList[] | null> => {
  try {
    const data = await contract?.call('getContractsByFreelancerId', [
      freelancerId,
    ]);

    return data.map((contract: any) => {
      return {
        id: contract[0].toNumber(),
        jobIdcurent: contract[1].toNumber(),
        title: contract[2],
        status: contract[3],
      };
    });
  } catch (err) {
    return null;
  }
};

export const smartContractService = {
  getDetailContractByJobId,
  getContractsByFreelancerId,
};
