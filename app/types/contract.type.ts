export enum ContractStatus {
  Created = 0,
  FreelancerSigned = 1,
  FreelancerCompleted = 2,
  ClientComfirm = 3,
  FreelancerCancel = 4,
  ClientCancel = 5,
}
