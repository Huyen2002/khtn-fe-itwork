import {
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/app/components/ui/table';
import { NotificationContext } from '@/app/providers/NotificationProvider';
import { GetAllReports, ResolveReports } from '@/app/services/adminService';
import { commonServices } from '@/app/services/common.services';
import { ReportList, ReportStatus, ReportType } from '@/app/types/reports.type';
import { Pagination, Select, Spin, Tag } from 'antd';
import { format } from 'date-fns';
import { ReactNode, useContext, useEffect, useState } from 'react';
import ResolveReportForm from './ResolveReportForm';

const PAGE_SIZE = 5;

function Reports() {
  const [loading, setLoading] = useState(false);
  const [reports, setReports] = useState<ReportList[]>([]);

  // Pagination
  const [currentPage, setCurrentPage] = useState(1);
  const [total, setTotal] = useState(0);

  // Delete report
  const [selectedReport, setSelectedReport] = useState<ReportList | undefined>(
    undefined
  );

  // Notification
  const { openNotificationWithIcon } = useContext(NotificationContext);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    getReports();
  }, [currentPage]);

  const getReports = async () => {
    if (!loading) {
      setLoading(true);
      try {
        const reports = await GetAllReports({
          page: currentPage,
          num: PAGE_SIZE,
          status: undefined,
        });
        if (reports.status == 200) {
          setReports(reports.data.data);
          setTotal(reports.data.total);
        } else openNotificationWithIcon('error', 'Lỗi', reports.message);
      } catch (error) {
        openNotificationWithIcon('error', 'Lỗi', error);
      } finally {
        setLoading(false);
      }
    }
  };

  const onPageChange = (page: number, pageSize: number) => {
    setCurrentPage(page);
  };

  const resolveReport = async (status: ReportStatus, results: string) => {
    if (selectedReport && !loading) {
      setLoading(true);
      try {
        const response = await ResolveReports(selectedReport.id, {
          resolve: status,
          results,
        });
        if (response.status == 200) {
          openNotificationWithIcon(
            'success',
            'Thành công',
            'Xử lý báo cáo thành công'
          );
          setReports((reports) =>
            reports.map((report) => {
              if (report.id != selectedReport.id) return report;
              report.status = status;
              report.results = results;
              return report;
            })
          );
          setSelectedReport(undefined);

          //Send notification
          const notification = await commonServices.sendNotication({
            title: 'Kết quả xứ lý báo cáo',
            message: `Kết quả: ${
              status == ReportStatus.Approve ? 'Đã xử lý' : 'Không xử lý'
            }\n\n${results}`,
            user_id: getReporter(selectedReport).id,
            user_type:
              selectedReport.type_id == ReportType.Client
                ? 'freelancer'
                : selectedReport.type_id == ReportType.Freelancer
                ? 'client'
                : selectedReport.client
                ? 'client'
                : 'freelancer',
            imagefile: null,
            linkable: 'Report',
            smail: 1,
          });
          console.log(notification);
        } else {
          openNotificationWithIcon('error', 'Lỗi', response.message);
        }
      } catch (error) {
        openNotificationWithIcon('error', 'Lỗi', error);
      } finally {
        setLoading(false);
      }
    }
  };

  const getReporter = (report: ReportList) => {
    return report.type_id == ReportType.Client
      ? report.freelancer
      : report.type_id == ReportType.Freelancer
      ? report.client
      : report.client ?? report.freelancer;
  };

  function displayReporter(report: ReportList): ReactNode {
    const reporter = getReporter(report);
    return (
      <>
        <TableCell>
          {!reporter!.avatar_url && (
            <div className='h-8 w-8 rounded-full border-solid border border-indigo-600' />
          )}
          {reporter!.avatar_url && (
            <img
              className='h-8 w-8 rounded-full border-solid border border-indigo-600'
              src={reporter!.avatar_url}
              alt={reporter!.username}
            />
          )}
        </TableCell>
        <TableCell className='text-center'>
          {reporter!.last_name && reporter!.last_name
            ? `${reporter!.last_name} ${reporter!.first_name}`
            : '-'}
        </TableCell>
        <TableCell>{reporter!.username}</TableCell>
        <TableCell>{reporter!.email}</TableCell>
      </>
    );
  }

  function getReported(report: ReportList): string {
    if (report.type_id == ReportType.Client) {
      return report.client.username;
    } else if (report.type_id == ReportType.Freelancer) {
      return report.freelancer.username;
    } else if (report.type_id == ReportType.Job) {
      return report.post.title;
    }
    return '';
  }

  const getReportType = (type: ReportType) => {
    if (type != ReportType.Other) {
      return ReportType[type];
    }
    return 'Loại khác';
  };

  return (
    <div>
      <table className='overscroll-none'>
        <TableHeader>
          <TableRow>
            <TableHead className='w-[100px] text-center font-medium'>
              STT
            </TableHead>
            <TableHead>Avatar</TableHead>
            <TableHead className='text-center'>Người báo cáo</TableHead>
            <TableHead className='text-center'>Username</TableHead>
            <TableHead className='text-center'>Email</TableHead>
            <TableHead className='text-center'>Bị báo cáo</TableHead>
            <TableHead className='text-center'>Ngày tạo</TableHead>
            <TableHead className='text-center'>Loại báo cáo</TableHead>
            <TableHead className='text-center'>Trạng thái</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody className='text-[14px]'>
          {reports.map((report, index) => (
            <TableRow
              key={`applied-job-item-${index}`}
              onClick={() => {
                setSelectedReport(report);
                setIsModalOpen(true);
              }}
            >
              <TableCell className='font-medium text-center'>
                {index + 1}
              </TableCell>
              {displayReporter(report)}
              <TableCell>{getReported(report)}</TableCell>
              <TableCell className='text-center'>
                {report.created_at
                  ? format(report.created_at, 'dd/MM/yyyy')
                  : '-'}
              </TableCell>
              <TableCell>{getReportType(report.type_id)}</TableCell>
              <TableCell className='w-[120px] text-center'>
                {!report.results ? (
                  <Tag color='volcano'>Chưa xử lý</Tag>
                ) : report.status == ReportStatus.Reject ? (
                  <Tag color='geekblue'>Không xử lý</Tag>
                ) : (
                  <Tag color='green'>Đã xử lý</Tag>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </table>
      {total != 0 && (
        <div className='flex justify-center pt-5'>
          <Pagination
            defaultCurrent={currentPage}
            total={total}
            pageSize={PAGE_SIZE}
            onChange={onPageChange}
          />
        </div>
      )}
      {loading && <Spin fullscreen />}
      {selectedReport && (
        <ResolveReportForm
          isOpen={isModalOpen}
          onCancel={() => {
            setIsModalOpen(false);
          }}
          report={selectedReport}
          onResolve={resolveReport}
        />
      )}
    </div>
  );
}

export default Reports;
